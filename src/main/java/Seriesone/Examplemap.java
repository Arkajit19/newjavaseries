package Seriesone;

import java.util.HashMap;
import java.util.Map;

public class Examplemap {
    public static void main(String[] args) {
        //Creating map of books
        HashMap<Integer,Book> kb1=new HashMap<>();

        //Creating books

        Book bks1=new Book(1,"History","Bipan Chandra","Pearson",3);
        Book bks2=new Book(2,"Geography","Hussain","Chaya",5);
        Book bks3=new Book(3,"Java Programming","Hortsman","Orailly",9);

        //Adding books to map

        kb1.put(1,bks1);
        kb1.put(2,bks2);
        kb1.put(3,bks3);

        //Traversing map

        for (Map.Entry<Integer,Book> l1: kb1.entrySet()){

             int k=l1.getKey();
              Book v=l1.getValue();
            System.out.println(k+" Details : ");
            System.out.println(v.id+"   "+v.name+"  "+v.author+"    "+v.publisher+"     "+v.quantity);
        }
    }
}
