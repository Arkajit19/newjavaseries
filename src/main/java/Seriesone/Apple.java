package Seriesone;

import java.util.concurrent.ConcurrentHashMap;

public class Apple {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer,String>cnhp=new ConcurrentHashMap<>();
        cnhp.put(1,"Gargi");
        cnhp.put(2,"arka");
        cnhp.put(3,"bani");
        cnhp.put(4,"Mohan");
        cnhp.put(5,"polo");
        cnhp.put(6,"kali");
        System.out.println("The map is : "+cnhp);
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println();
        cnhp.compute(5,(key,val)->val+" "+100);
        System.out.println(cnhp);
    }
}
