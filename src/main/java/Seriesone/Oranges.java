package Seriesone;

import java.util.concurrent.ConcurrentHashMap;

public class Oranges {

    public static void main(String[] args) {

        ConcurrentHashMap<Integer,String>mplp=new ConcurrentHashMap<>();
        mplp.put(1,"Arkajit");
        mplp.put(2,"Rakim");
        mplp.put(3,"Hamid");
        mplp.put(4,"Manu");
        mplp.put(5,"Nasim");
        System.out.println("The old map is : "+mplp);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println();
        mplp.compute(4,(k,v)->v.concat(" Title"));
        System.out.println("The new map is "+mplp);
    }
}
