package Seriesone;

import java.util.HashMap;
import java.util.Map;

public class Pune {
    public static void main(String[] args) {
        HashMap<Integer, String>map=new HashMap<>();
        map.put(1,"Apple");
        map.put(2,"Mango");
        map.put(3,"Lemon");
        map.put(4,"Guava");
        map.put(5,"Bananna");
        for (Map.Entry k1:map.entrySet()){

            System.out.println("The key is "+k1.getKey()+" . The value is "+k1.getValue()+" . ");
        }
        System.out.println("---------------------------------------------------------------------------");
        System.out.println();
        map.putIfAbsent(7,"Potato");
        for (Map.Entry p1:map.entrySet()){

            System.out.println("The key is "+p1.getKey()+" . The value is "+p1.getValue()+" . ");
        }
        System.out.println("------------------------------------------------------------------------------------");

        System.out.println();

        HashMap<Integer,String>map1=new HashMap<>();
        map1.put(9,"Carrot");
        map1.putAll(map);
        for (Map.Entry t1:map1.entrySet()){

            System.out.println("The key is "+t1.getKey()+" . The value is "+t1.getValue()+" . ");
        }


    }
}
