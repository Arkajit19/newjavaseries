package Seriesone;

import java.util.HashMap;
import java.util.Map;

public class West_Bengal {

    public static void main(String[] args) {

        HashMap<Integer,String>map3=new HashMap<>();
        map3.put(1,"Suzuki");
        map3.put(2,"Honda");
        map3.put(3,"Datsun");
        map3.put(4,"Skoda");
        System.out.println("The initial list is : ");
        for (Map.Entry j1:map3.entrySet()){

            System.out.println("The key is "+j1.getKey()+" . The value is "+j1.getValue()+" . ");

        }
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println();
         map3.remove(2);
        System.out.println("The updated list is : ");
        for (Map.Entry j2:map3.entrySet()){

            System.out.println("The key is "+j2.getKey()+" . The value is "+j2.getValue()+" . ");

        }
        System.out.println("---------------------------------------------------------------------------");
        System.out.println();
        System.out.println("The second updated list is : ");
        map3.replace(3,"Rollsroyce");
        for (Map.Entry n1: map3.entrySet()){

            System.out.println("The key is "+n1.getKey()+" . The value is "+n1.getValue()+" . ");

        }
        System.out.println("--------------------------------------------------------------------------");
        System.out.println();
        System.out.println("The third updated list is : ");
        map3.replace(4,"Skoda","Castrol");
        for (Map.Entry b1:map3.entrySet()){

            System.out.println("The key is "+b1.getKey()+" . The value is "+b1.getValue()+" . ");
        }

    }
}
