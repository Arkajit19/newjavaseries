package codehub;

import java.util.HashMap;

public class Absentif {

    public static void main(String[] args) {

        HashMap<String,Integer>pm=new HashMap<>();
        pm.put("Tuna",5);
        pm.put("Rakim",2);
        pm.put("Aman",10);
        pm.put("Bunny",21);
        System.out.println("The old map is : "+pm);
        System.out.println();
        System.out.println("-----------------------------------------------------------------------");
        pm.computeIfAbsent("Tata",val->68+76);
        pm.computeIfAbsent("Maruti",val->60*34);
        System.out.println("The updated map is : ");
        System.out.println(pm);

    }
}
