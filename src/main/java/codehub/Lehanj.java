package codehub;

import java.util.ArrayDeque;
import java.util.Deque;

public class Lehanj {

    public static void main(String[] args) {

        Deque<String>deque=new ArrayDeque<>();
        System.out.println("Old que :");
        deque.add("Hamid");
        deque.add("Samir");
        deque.add("Aman");
        deque.add("Biman");
        deque.add("Chetan");
        deque.add("Rakesh");
        for (String d1:deque){

            System.out.println(d1);
        }
        System.out.println();
        System.out.println("---------------------------------------------------------------------");
        System.out.println();
        System.out.println("After adding element in first : ");
        deque.offerFirst("Monali");
        for (String l1:deque){

            System.out.println(l1);
        }

        System.out.println();
        System.out.println("=============================================================================");
        System.out.println();
        System.out.println("After removing from last : ");
        deque.pollLast();
        for (String k1:deque){

            System.out.println(k1);
        }
    }
}
