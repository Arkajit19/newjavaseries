package codehub;

import java.util.Iterator;
import java.util.PriorityQueue;

public class Fulltop {
    public static void main(String[] args) {
        PriorityQueue<String>pqu1=new PriorityQueue<>();
        pqu1.add("Aman");
        pqu1.add("Binay");
        pqu1.add("Lisa");
        pqu1.add("Giga");
        pqu1.add("Jeesan");
        pqu1.add("Kakoli");
        pqu1.add("Nahim");
        System.out.println("Head is : "+pqu1.element());
        System.out.println("Head is : "+pqu1.peek());
        Iterator<String>iter= pqu1.iterator();
        while (iter.hasNext()){

            System.out.println(iter.next());
        }
        System.out.println("-------------------------------------------------------------------------------");
        pqu1.poll();
        Iterator<String>ltre= pqu1.iterator();
        while (ltre.hasNext()){

            System.out.println(ltre.next());
        }

        System.out.println("-----------------------------------------------------------------------------");
    }
}
