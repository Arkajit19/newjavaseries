package codehub;

import java.util.HashMap;

public class Sampleone {

    public static void main(String[] args) {

        HashMap<Integer,String>pm1=new HashMap<>();
        pm1.put(1,"arka");
        pm1.put(2,"kolkata");
        pm1.put(3,"ketab");
        pm1.put(4,"sami");
        pm1.put(5,"bihar");
        System.out.println("The old map is : "+pm1);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------");
        System.out.println();

        pm1.computeIfPresent(4,(key,val)->val+" title");
        System.out.println("The updated map is : "+pm1);

    }
}
