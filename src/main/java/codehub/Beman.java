package codehub;

import java.io.File;
import java.io.FileInputStream;
import java.io.SequenceInputStream;

public class Beman {

    public static void main(String[] args) {

        try {
            FileInputStream fin1=new FileInputStream("C:\\Users\\arkaj\\Desktop\\codingworld\\newgitlab\\newjavaseries\\src\\main\\java\\codehub\\sample34.txt");
            FileInputStream fin2=new FileInputStream("C:\\Users\\arkaj\\Desktop\\codingworld\\newgitlab\\newjavaseries\\src\\main\\java\\codehub\\Testsample.txt");
            SequenceInputStream sqnin=new SequenceInputStream(fin1,fin2);
            int j;
            while ((j=sqnin.read())!=-1){

                System.out.print((char) j);
            }
            System.out.println();
            fin1.close();
            fin2.close();
            sqnin.close();
        }
        catch (Exception e){

            System.out.println(e);
            System.out.println("Please retry !!!");
        }

    }
}
